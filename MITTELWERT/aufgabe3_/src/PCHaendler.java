import java.util.Scanner;

public class PCHaendler {

	
		
		static Scanner myScanner = new Scanner(System.in);
	
		public static void main(String[] args) {
			
			// Benutzereingaben lesen
	//		System.out.println("was m�chten Sie bestellen?");
	//		String artikel = myScanner.next();
			
			String artikel = liesString("was möchten Sie bestellen");
	
	//		System.out.println("Geben Sie die Anzahl ein:");
	//		int anzahl = myScanner.nextInt();
	//		
			int anzahl = liesInt("Geben Sie die Anzahl ein:");
			
	//		System.out.println("Geben Sie den Nettopreis ein:");
	//		double preis = myScanner.nextDouble();
	//		
			double preis = liesDouble("Geben Sie den Nettopreis ein:");
			
	//		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	//		double mwst = myScanner.nextDouble();
			
			double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
	
			// Verarbeiten
			//double nettogesamtpreis = anzahl * preis;
			//double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			
			double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
			double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);
			
			myScanner.close();
			
			// Ausgeben
	
			System.out.println("\tRechnung");
			System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
			System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	
		}
	
		public static String liesString(String text) {
			System.out.println(text);
			String gelesenerString = myScanner.next();
			return gelesenerString;
		}
		
		public static int liesInt(String text) {
			System.out.println(text);
			int geleseneInt = myScanner.nextInt();
			return geleseneInt;
		}
		
		public static double liesDouble(String text) {
			System.out.println(text);
			double geleseneDouble = myScanner.nextDouble();
			return geleseneDouble;
		}
		
		public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
			double nettogesamtpreis = anzahl * nettopreis;
			return nettogesamtpreis;
		}
		
		public static double berechneGesamtbruttopreis(double nettogesamtpreis,
				double mwst) {
			double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
			return bruttogesamtpreis;
		}
	}