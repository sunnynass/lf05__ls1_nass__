package Arbeitsblatt2;

public class Aufgabe3 {
    public static void main(String[] args) {
        
        System.out.printf("%-12s|", "Fahrenheit" );
        System.out.printf("%10s\n", "Celsius" );
        System.out.println("-----------------------" );

            System.out.printf("%-12s|", "-20" );
            double kommaZahl1 = -28.8889; 
            kommaZahl1 = Math.round(kommaZahl1 * 100.0) / 100.0;
		    System.out.printf("%10s\n",kommaZahl1);
		
            System.out.printf("%-12s|", "-10" );
            double kommaZahl2 = -23.3333; 
            kommaZahl2 = Math.round(kommaZahl2 * 100.0) / 100.0;
            System.out.printf("%10s\n",kommaZahl2);
            
            System.out.printf("%-12s|", "+0" );
            double kommaZahl3 = -17.7778; 
            kommaZahl3 = Math.round(kommaZahl3 * 100.0) / 100.0;
            System.out.printf("%10s\n",kommaZahl3);
            
            System.out.printf("%-12s|", "+20" );
            double kommaZahl4 = -6.6667; 
            kommaZahl4 = Math.round(kommaZahl4 * 100.0) / 100.0;
            System.out.printf("%10s\n",kommaZahl4);
            
            System.out.printf("%-12s|", "+30" );
            double kommaZahl5 = -1.1111; 
            kommaZahl5 = Math.round(kommaZahl5 * 100.0) / 100.0;
            System.out.printf("%10s\n",kommaZahl5);



            
    } 
    
}
