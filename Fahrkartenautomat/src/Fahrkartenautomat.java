﻿import java.util.Scanner;

class Fahrkartenautomat {

    public static void main(String[] args) {

        char bestätigung;
        Scanner tastatur = new Scanner(System.in);
        do{
            double zuZahlenderBetrag = fahrkartenbestellungErfassen();

            double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);

            rueckgeldAusgeben(rückgabebetrag);

            fahrkartenAusgeben();
            fahrkartenNichtVergessen();

            System.out.println ("möchten sie weitere Tickets kaufen? ");
            bestätigung = tastatur.next().charAt(0);

        } while (bestätigung=='j'||bestätigung=='J');
        
        if (bestätigung=='n'||bestätigung=='N');
        System.out.println("bis zum nächsten mal ");
        
    }
    //Fahrkarten Array für Namen und Preise der Karten
    //durch die 2 Aarrays ist es einfacher änderungen vor zu nehmen und der Code wirkt ordentlicher.
    //nachteile kann ich hier leider keine erkennen.
    public static double fahrkartenbestellungErfassen() {

        Scanner sc = new Scanner(System.in);
        boolean isValid = true;
        String[] farkartenbezeichnung = new String[10];
        double[] preis = new double[10];
        int anzahl = 0;
        int ticket = 0;

        farkartenbezeichnung[0] = "1 Einzelfahrschein Berlin AB";
        farkartenbezeichnung[1] = "2 Einzelfahrschein Berlin BC";
        farkartenbezeichnung[2] = "3 Einzelfahrschein Berlin AB";
        farkartenbezeichnung[3] = "4 Kurzstrecke";
        farkartenbezeichnung[4] = "5 Tageskarte Berlin AB";
        farkartenbezeichnung[5] = "6 Tageskarte Berlin BC";
        farkartenbezeichnung[6] = "7 Tageskarte Berlin ABC";
        farkartenbezeichnung[7] = "8 Kleingruppen-Tageskarte Berlin AB";
        farkartenbezeichnung[8] = "9 Kleingruppen-Tageskarte Berlin BC";
        farkartenbezeichnung[9] = "10 Kleingruppen-Tageskarte Berlin ABC";

        preis[0] = 2.90;
        preis[1] = 3.30;
        preis[2] = 3.60;
        preis[3] = 1.90;
        preis[4] = 8.60;
        preis[5] = 9.00;
        preis[6] = 9.60;
        preis[7] = 23.50;
        preis[8] = 24.30;
        preis[9] = 24.90;

        do {
            System.out.println("Bitte wählen Sie Ihr Ticket");
            for (int i = 0; i < farkartenbezeichnung.length; i++){
                System.out.println(farkartenbezeichnung[i]);
            }
            ticket = sc.nextInt();

            if (ticket < 1 || ticket > farkartenbezeichnung.length){
                    System.out.println("kein gültiges Ticket");
                    isValid = false;
            } else {
                isValid = true;
            }
            
            System.out.println("Anzahl: ");
            anzahl = sc.nextInt();

            double betrag = anzahl * preis [ticket - 1];
            
            System.out.println("Der Preis beträgt" + betrag + "Euro");

        
            
    }   while (!isValid);

        return (anzahl * preis[ticket - 1]);
    }

    



    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
        Scanner tastatur = new Scanner(System.in);
        double eingezahlterGesamtbetrag;
        double eingeworfeneMünze;
        double rückgabebetrag = 0.0;

        eingezahlterGesamtbetrag = 0.0;
        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f Euro \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));

            System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMünze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMünze;

            rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
        }
        return rückgabebetrag;
        
    }

    public static void rueckgeldAusgeben(double rückgabebetrag) {
        if (rückgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro \n", rückgabebetrag);
            System.out.println("wird in folgenden Münzen ausgezahlt:");

            while (rückgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                System.out.println("2 EURO");
                rückgabebetrag -= 2.0;
            }
            while (rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                System.out.println("1 EURO");
                rückgabebetrag -= 1.0;
            }
            while (rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                System.out.println("50 CENT");
                rückgabebetrag -= 0.5;
            }
            while (rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                System.out.println("20 CENT");
                rückgabebetrag -= 0.2;
            }
            while (rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                System.out.println("10 CENT");
                rückgabebetrag -= 0.1;
            }
            while (rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                System.out.println("5 CENT");
                rückgabebetrag -= 0.05;
            }
        }
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            try {
                Thread.sleep(250);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("\n\n");
    }

    public static void fahrkartenNichtVergessen() {
        System.out.println("Vergessen Sie Ihre Fahrkarten nicht" + "\n" + "Einen schönen Tag noch");
    }

}
