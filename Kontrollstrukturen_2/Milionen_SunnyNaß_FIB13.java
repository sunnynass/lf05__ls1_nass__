import java.util.Scanner;
public class Milionen_SunnyNaß_FIB13 {
    public static void main(String[] Args) {

		Scanner sc = new Scanner(System.in);
		char eingabe = 'j';
		double anlageKapital, zinssatz;
		int i = 1;

		while (eingabe == 'j') {

			// Eingabe
			System.out.print("wie viel kapital möchten sie anlegen: ");
			anlageKapital = sc.nextDouble();

			System.out.print("Bitte geben Sie den Zinssatz an: ");
			zinssatz = sc.nextDouble();
			zinssatz = zinssatz / 100 + 1;

			// Verarbeitung
			while (anlageKapital < 1000000) {
				anlageKapital = anlageKapital * zinssatz;
				i++;
			}
			// Ausgabe
			System.out.println("Nach " + i + " Jahren überschreitet das Anlagekapitel die Schwelle von 1.000.000 Euro");
			
			System.out.println("Möchten Sie einen weiteren Durchlauf starten? (j/n)");
			eingabe = sc.next().charAt(0);
		}
		sc.close();
	}
}

