import java.util.Scanner;
public class Matrix_SunnyNaß_FIB13 {
    public static void main(String[] args) {

		Scanner eingabe = new Scanner(System.in);

		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl = eingabe.nextInt();
		int i = 0;
		
		System.out.println("");
		
		while (i < 100) {
			
			int ersteStelle = i / 10;
			int zweiteStelle = i % 10;
			
			
			if (i % zahl == 0) 
				System.out.printf("%4s", "*");
			
			else if ( ersteStelle == zahl || zweiteStelle == zahl) 
				System.out.printf("%4s", "*");
			
			else if ( ersteStelle + zweiteStelle == zahl) 
				System.out.printf("%4s", "*");
			
			else 
				System.out.printf("%4d", i);
			
			i++;
			
			if (i % 10 == 0 && i != 0) 
				System.out.println();
        }
        eingabe.close();
    }
}

